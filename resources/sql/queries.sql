-- :name save-book! :! :n
-- :doc "save a book"
insert into guestbook2
(bookname,  author_id)
values (:bookname , :author-id);

-- :name select-books :? :*
-- :doc "Select all Books"
SELECT * FROM guestbook2



-- :name update-book :! :n
-- :doc "updates bookname"
UPDATE guestbook2
SET bookname = :newbookname,
    author_id = :author-id
WHERE id = :id

-- :name delete-book! :! :n
-- :doc "delete a book"
DELETE FROM guestbook2
WHERE bookname = :bookname
AND id = :id

-- :name save-author! :! :n
-- :doc "Save a book"
INSERT INTO author
(authorname)

VALUES (:authorname)


-- :name select-authors :? :*
-- :doc "Select all authors"
SELECT * FROM author

-- :name update-author :! :n
-- :doc "updates author name"
UPDATE author
SET authorname = :newauthorname
WHERE id = :id

-- :name delete-author! :! :n
-- :doc "delete a author name"
DELETE FROM author
WHERE authorname = :authorname
AND id = :id
