(ns guestbook2.routes.home
  (:require [guestbook2.layout :as layout]
            [compojure.core :refer [defroutes GET POST]]
            [ring.util.http-response :as response]
            [clojure.java.io :as io]
            [guestbook2.db.core :as db]))


(defroutes home-routes
  (GET "/" [] (layout/render "home.html"))
   (POST "/add-book" [] (fn [request] {:response (db/save-book! (:params request))}))
   (GET "/books" [] (db/select-books))
   (GET "/authors" [] (db/select-authors))
   (POST "/add-author" [] (fn [request] {:response (db/save-author! (:params request))})))
