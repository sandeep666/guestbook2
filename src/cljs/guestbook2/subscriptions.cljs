(ns guestbook2.subscriptions
  (:require [re-frame.core :refer [reg-sub]]))

(reg-sub
  :page
  (fn [db _]
    (:page db)))

(reg-sub
  :docs
  (fn [db _]
    (:docs db)))

(reg-sub
  :book-list
  (fn [db _]
    (:book-list db)))

(reg-sub
  :author-list
  (fn [db _]
    (:author-list db)))




(reg-sub
 :current-book
 (fn [db _]
   (:current-book db)))


(reg-sub
 :current-author
 (fn [db _]
   (:current-author db)))
