(ns guestbook2.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [secretary.core :as secretary :refer [dispatch!]]
            [goog.events :as events]
            [goog.history.EventType :as HistoryEventType]
            [markdown.core :refer [md->html]]
            [ajax.core :refer [GET POST DELETE]]
            [guestbook2.ajax :refer [load-interceptors!]]
            [guestbook2.handlers]
            [reagent-material-ui.core :as md]
            [guestbook2.subscriptions]
            [re-frisk.core :refer [enable-re-frisk!]])
  (:use [incanter core stats charts])
  (:import goog.History))

(defn nav-link [uri title page collapsed?]
  (let [selected-page (rf/subscribe [:page])]
    [:li.nav-item
     {:class (when (= page @selected-page) "active")}
     [:a.nav-link
      {:href uri
       :on-click #(reset! collapsed? true)} title]]))

(defn navbar []
  (r/with-let [collapsed? (r/atom true)]
    [:nav.navbar.navbar-dark.bg-primary

     [:button.navbar-toggler.hidden-sm-up
      {:on-click #(swap! collapsed? not)} "☰"]
     [:div.collapse.navbar-toggleable-xs
      (when-not @collapsed? {:class "in"})
      [:a.navbar-brand {:href "#/"} "Book list"]
      [:ul.nav.navbar-nav
       [nav-link "#/" "books" :home collapsed?]
       [nav-link "#/about" "About" :about collapsed?]]]]))






(defn home-page []

  (let [book-list (rf/subscribe [:book-list])
        author-list (rf/subscribe [:author-list])
        book (rf/subscribe [:current-book])
        author (rf/subscribe [:current-author])]
    #_(GET "/authors" {:handler #(rf/dispatch [:assoc-in [:author-list] %])})
    (fn []




      [:div.form-group
       [:label "pick a book"]
       [:select {:on-change #(rf/dispatch [:assoc-in [book]
                                           (first (filter (fn [book]
                                                            (= (str (:id book)) (-> % .-target .-value)))
                                                          @book-list))])}
        (for [book @book-list]
          ^{:key (:id book)} [:option {:value (:id book)} (:bookname book)])]

       [:input {:type     "button"
                :value    "Go"
                :on-click #(rf/dispatch [:set-active-page :book])}]

       [:input {:type     "button"
                :value    "Add book"
                :on-click #(rf/dispatch [:set-active-page :add-book])}]


       [:br][:label "pick an author"]


       [:select {:on-change #(rf/dispatch [:assoc-in [author]
                                           (first (filter (fn [author]
                                                             (= (str (:id author)) (-> % .-target .-value)))
                                                          @author-list))])}
        (for [author @author-list]
          ^{:key (:id author)} [:option {:value (:id author)} (:authorname author)])]

       [:input {:type     "button"
                :value    "Add author"
                :on-click #(rf/dispatch [:set-active-page :add-author])}]


       ])))





(defn view-book []
  (let [book (rf/subscribe [:current-book])
        author (rf/subscribe [:current-author])]
    (fn []
      [:div
       [:p "book name : " (:bookname @book)]
       [:p "book ID : " (:id @book)]
       [:p "author name: " (:authorname (first (filter #(= (:id %) (:author-id @book)) @author)))]
       [:input {:type     "button"
                :value    "back"
                :on-click #(dispatch! "/")}]

       [:input {:type     "button" :value " update"
                :on-click #(dispatch! "/update-book")}
        [:input {:type "button" :value "delete" :on-click #(DELETE "/delete-book" {:params  @book
                                                                                   :handler (fn [response] (dispatch! "/"))})}]]])))





(defn add-book []
  (let [book (rf/subscribe [:current-book])
        author (rf/subscribe [:current-author])
        author-list (rf/subscribe [:author-list])]


      [:div
       [:p "Book name"
        [:p (histogram (sample-normal 1000))]
        [:input {:type "text" :on-change #(rf/dispatch [:assoc-in [:current-book :bookname] (-> % .-target .-value)])}]

        [:input {:type     "button" :value "Save"
                 :on-click #(POST "/add-book" {:params  @book
                                               :handler (fn [response] (rf/dispatch [:set-active-page :home]))})}]]


       [:label "pick an author"]
       [:select {:on-change #(rf/dispatch [:assoc-in [:current-book :author-id]
                                           (:id (first (filter (fn [author]
                                                                 (= (str (:id author)) (-> % .-target .-value)))
                                                               @author-list)))])}
        (for [author @author-list]
          ^{:key (:id author)} [:option {:value (:id author)} (:authorname author)])]

       [:input {:type     "button"
                :value    "Add author"
                :on-click #(rf/dispatch [:set-active-page :add-author])}]


       [:input {:type "button" :value "back" :on-click #(rf/dispatch [:set-active-page  :home]) }]]))






(defn add-author []
  (let [author (rf/subscribe [:current-author])
        author-list (rf/subscribe [:author-list])]
    [:div
     [:p "author name"
      [:input {:type "text" :on-change #(rf/dispatch [:assoc-in [:current-author :authorname] (-> % .-target .-value)])}]]
     [:input {:type "button" :value "back" :on-click #(rf/dispatch [:set-active-page :home]) }]
     [:input {:type "button" :value "Save"
              :on-click #(do (POST "/add-author" {:params  @author
                                                  :handler (fn [response] (rf/dispatch [:set-active-page :home]))})
                             (GET "/authors" {:handler (fn [resp] (rf/dispatch [:assoc-in [:author-list] resp]))}))}]]))





(def pages
  {:home #'home-page
   :book #'view-book
   :add-book #'add-book
   :add-author #'add-author})

(defn page []
  [:div
   [navbar]
   [(pages @(rf/subscribe [:page]))]])

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")

(secretary/defroute "/" []
  (rf/dispatch [:set-active-page :home]))

(secretary/defroute "/book" []
  (rf/dispatch [:set-active-page :book]))

(secretary/defroute "/add-book" []
   (rf/dispatch [:set-active-page :add-book]))

(secretary/defroute "/add-author" []
   (rf/dispatch [:set-active-page :add-author]))


;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
      HistoryEventType/NAVIGATE
      (fn [event]
        (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

;; -------------------------
;; Initialize app
(defn fetch-books-and-authors! []
  (do
    (GET "/books" {:handler #(rf/dispatch [:assoc-in [:book-list] %])})
    (GET "/authors" {:handler #(rf/dispatch [:assoc-in [:author-list] %])})))


(defn mount-components []
  (r/render [#'page] (.getElementById js/document "app")))

(defn init! []
  (rf/dispatch-sync [:initialize-db])
  (fetch-books-and-authors!)
  (enable-re-frisk!)
  (load-interceptors!)
  (hook-browser-navigation!)
  (mount-components))
